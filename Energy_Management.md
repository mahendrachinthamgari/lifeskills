# Manage Energy not Time

### 1. What are the activities you do that make you relax - Calm quadrant?

Some activities that make me relax are -

* Playing Volleyball.
* Getting into sleep.
* Listening to music.
* Watching movies.
* Reading books.

### 2. When do you find getting into the Stress quadrant?

When I have a lot of work pending, racing against time to complete tasks before deadlines, getting stuck in solving problems and not understanding concepts, these are the times I feel like giving up and feel like a loser.

### 3. How do you understand if you are in the Excitement quadrant?

When I am anticipating the start of a rewarding and joyful experience like at the beginning of a football match, or when I am expecting some good news, I umderstand that I am in the excitement quadrant.

### 4. Paraphrase the Sleep is your Superpower video in detail

Sleep is a very important activity in life. It is essential to sleep enough to stay in good health and spirits. Studies have shown that people with less sleep are more prone to have health issues like increased chances of heart attacks, poor immunity, and early onset of aging.

### 5. What are some ideas that you can implement to sleep better?

Relaxing the body, fixing a particular time to sleep, an optimal room temperature, frequent exercise, and having a fixed time to have dinner can help with better sleep.

### 6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points

There are several brain-changing benefits of exercise. Some of them are-

* Maintaining focus for longer stretches of time than earlier.
* Slower onset of ageing, and better chances of avoiding incurable diseases.
* Helps reduce obesity and makes you feel good due to releasae of dopamine.
* Helps in tackling stress.

### 7. What are some steps you can take to exercise more?

Building a habit is more important than just having motivation because, with a habit, the brain automatically ensures the regularity of exercise, thereby making us stronger mentally and physically. It only takes some initial effort and willingness to do so. One can easily add some physical activities like running an extra mile, freestyle jumping, hand movements, and some aerobics to exercise the heart and build up stamina.
