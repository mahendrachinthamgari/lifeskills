# Grit and Growth Mind set

## 1. Grit

### Q1. Paraphrase (summarize) the video in a few lines. Use your own words

In the video, speaker Angela Lee Duckworth, has studied psychology to test and measure the talent in the childrens. She did so many researches and find out that:

* Those who are tenacious can acheive more than those who are just talented.
* Studies show that gritty people become sucessful despite facing trying circumstances.
* Keeping our promises can help us develop grit.
* Talent is inversely correlated with grit.
* Grit can only be developed by adopting an attitude of personal growth.

Few things about **Grit**

* Grit is a passion and preseverance for very long term goal.
* Grit is stamina.
* Grit is sticking with our feature, day in, day out.
* Grit is living life like it's a marathon, not a spirit.
* Talent doesn't make you gritty.

### Q2. What are your key takeaways from the video to take action on?

Key takeqways from the video:

* Building passion and perseverance for goals makes it easier to achieve them.
* Only talent will not suffice to achieve the objectives.
* Talent combined with grit makes achieving the goal easy.
* With grit, everyone can achieve anything they want in their life.

## 2. Introduction to Growth Mindset

### Question 3

#### Paraphrase (summarize) the video in a few lines in your own words

* People have two different mental states.
* When it comes to learning, these mindsets are particularly crucial.
* Some individuals possess a fixed attitude, while others possess a growth mindset.
* Individuals with fixed minds think that intelligence and talents are predetermined.
* Those with a fixed mindset think that learning and growth are impossible or unnecessary.
* They think they don't have control over their skills.
* Those who have a growth mindset think that intelligence and skills can be improved.
* As a result, those who are good at something are good at it because they developed the skill, and others who aren't good at it are bad at it because they didn't put in the effort.
* They think they have control over their skills.
* Those with a growth mindset think that abilities can be developed.
* And those with a growth mindset do think they can learn and develop.

### Question 4

#### What are your key takeaways from the video to take action on?

1. The growth mindset is the idea that with effort, we can develop our brains and increase our intelligence.
2. People who have a growth mentality can value what they are doing regardless of the result.
3. The idea that we are fixed in our ways from birth is known as a fixed mindset.
4. Everything in the fixed attitude is based on the result.
It's all for naught if we fail or aren't the best.

## 3. Understanding Internal Locus of Control

### Question 5

#### What is the Internal Locus of Control? What is the key point in the video?

**Internal locus of control**

: They considered that they had total control over the factors that affected their results. They concluded that it was their attention and extra effort that had allowed them to score well on the puzzles.If we wish to feel motivated constantly, we need to believe that we are in charge of our lives and are accountable again for events that occur to us.incredibly driven person to be able to deal with countless disappointments every single day.

**Key Point**

The need of keeping a positive mindset, being motivated, and searching for justifications instead of justifications when conducting out tasks were all emphasized in this video.The lesson this movie taught us was how to be a doer, not really an explanation maker, and to recognize that our biggest barrier to achievement isn't physical, but rather emotional.

## 4. How to build a Growth Mindset

### Question 6

#### Paraphrase (summarize) the video in a few lines in your own words

* A growth mindset begins with the fundamental first principle of life, which is to have confidence in our capability for problem-solving.
* We must consider in a manner that will allow us to come up with a solution if we have a difficult goal but are unsure of how to achieve it.
* Secondly, we need to question our assumptions and negative viewpoints.
* Thirdly, our life curriculum has to include lessons on being receptive to new ideas.
* Respect the conflict because it will make us stronger and give us the ability to remain calm in the midst of chaos.

### Question 7

#### What are your key takeaways from the video to take action on?

1. Identify, consider, and accept all of our shortcomings.
2. Dispelling the misconception that a challenge is inherently bad is a component of cultivating a growth mindset.
3. Accept problems and see them as valuable learning opportunities that we wouldn't otherwise acquire.
4. Establish definite, practical objectives based on our purpose and passion.
5. And be sure to give ourselves enough time to properly defeat them.

## 5. Mindset - A MountBlue Warrior Reference Manual

### Question 8

#### What are one or more points that you want to take action on from the manual?

1. I will understand each concept properly.
2. I will not leave my code unfinished till I complete the following checklist:
    * Make it work.
    * Make it readable.
    * Make it modular.
    * Make it efficient.
3. I will follow the steps required to solve problems:
    * Relax
    * Focus - What is the problem? What do I need to know to solve it?
    Understand - Documentation, Google, Stack Overflow, Github
    * Issues, Internet
    * Code
    * Repeat
