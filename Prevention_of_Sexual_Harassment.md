# Prevention of Sexual Harassment

## Behaviour that causes Sexual Harassment

: Any unwelcome verbal, visual or physical conduct of a sexual nature that is severe or pervasive and affects working condition or creates a hostile work environment.

Sexual Harassment is divided into three forms.

1. Verbal Sexual Harassment.
2. Visual Sexual Harassment.
3. Physical Sexual Harassment.

**Verabal Sexual Harassment**

Comments about clothing, a person's body, sexual or gender-based jokes or remarks, requesting sexual favors, repeatedly asking a person out, as well as innuendos, threats, spreading rumours about a person's personal or sexual life, or using foul and abusive language, comes under verbal sexual harassment.

**Visual Sexual Harassment**

Visual Sexual Harassment includes abuse posters, drawings, pictures, screen savers, cartoons, emails, or texts of a sexual nature.

**Physical Sexual Harassment**

Physical Sexual Harassment includes sexual assault, impeding or blocking movement, inappropriate touching such as kissing, hugging, patting, stroking, or rubbing, sexual gesturing or leering or staring,

## In case I face or witness any incident or repeated incidents of Sexual Harassment behaviour

**Case:- I face such behaviour**

In the event that I am subjected to sexual harassment, I will observe the behaviour to determine whether the harasser is doing so knowingly or unknowingly; if they are doing so unknowingly, I will warn them. If they repeat the same behaviour, I will inform the higher authorities about their behavior. If they are doing it knowingly, I will immediately inform higher authorities about their behaviour. If they do not change thier behaviour regandingless of any warnings from me and higher authorities, then I will file a case against them under Sexual Harassment Act.

**Case:- I withness any sexual harassment behaviour**

In the event that I witness sexual harassment, I will immediately support the victim and warn the harasser. If the harasser is my coworker, I will warn the harasser to change their behavior. If they do the same, I will inform the higher authorities. If the harasser is my superior, I will notify the appropriate authorities about his or her behavior. Regardless of any warnings, if they continue the same behaviour, I will file a case against them under the Sexual Harassment Act.
