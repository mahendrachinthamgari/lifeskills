# Focus Management

### What is Deep Work?

According to Cal Newport Deep work is termed as focusing without any distraction on a congitively demanding task.

* Coding is the classic example of deep work because, we got this problem, we need to solve, we have all of these tools and we have to combine them all creatively. There it needs more focus.

### Paraphrase all the ideas in the above videos and this one in detail.

According to Cal Newport, Optimal duration for deep work is at least 1 hour.

Example: When we are coding for 20mins we might only getting 10 or 15 minutes of actual work, so at least 1 hour is the optimal duration for deep work. If work deeply for 1 hour we might getting a 45 to 50 minutes of work.

Deadlines are clear motivational signal but in the sort term we do get an effect like that in time blocking. The stron effect we get by saying this is the exact time i'm going to work on this is that we dont't have the debate with ourself every three minutes about taking brea. So if we have deadlines we will spend all the time by completing the perticular task. So deadlines are good for productivity.

Three deep work strategies that we can incorporate into our schedule to heighten our ability to focus and produce results that are hard to replicate.

1. Schedule Distractions: 

* Schedule distraction periods at home and at work. Most of us allow ourselves to go online at any moment and check our phone whenever buzzes or dings but doing so is training our brain to avoid deep work. To overcome this distraction, put a note pad nearby and put down the next distraction break we will have hold our focus until that time.

2. Deep Work Ritual:

* Develop a deep work ritual is the easiest way to consistently start deep work session is to transform them into a simple regular habbit.

3. Evening Shutdown:

* Third strategy to cultivate deep work in our life is to have a daily shutdown complete ritual. Sleep is the price we need to pay in order to do deep work.

### How can you implement the principles in your day to day life?

I can implement the principles in my day to day life by following these steps:

* I will follow the three Deep Work strategies.
* I will do Deep Work for atleast one hour.
* I will schedule the distractions by doing certains things like keeping mobile in silent mode.
* I will follow Evening Shutdown strategy.
* I will sleep for sufficient time.


### Your key takeaways from the video.


Some key points of the video :

* Without social media, we still make friends.
* Without social media, we can still know what's going in the world.
* Without social media, we can be better.
* Social media seems to be little bit dangerious.
* Social Media wastes too much time and is unhealthy for mind and body.
* Social Media keeps distracting as it is designed to keep your attention for as long as possible.
* Use of social media is bad for one's mental health as it leads to comparison of lifestyles which can depress a person.