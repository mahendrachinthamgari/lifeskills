# Good Practices for Software Development

## Question 1

### What is your one major takeaway from each one of the 6 sections. So 6 points in total

* Sometimes despite all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page.

* Do not miss calls. If you cannot talk immediately, receive the call and inform them you will call in back in 5-10 min, and call them back. This is a much better alternative than letting it go down as a missed call.

* The way you ask a question determines whether it will be answered or not. You need to make it very easy for the person to answer your question. Messages like the database is not connecting, need your help or the build is not working, can you help me out, will not get you the answers you are looking for.

* Join the meetings 5-10 mins early to get some time with your team members.

* Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.

* Make sure you do some exercise to keep your energy levels high throughout the day.

## Question 2

### Which area do you think you need to improve on? What are your ideas to make progress in that area?

These are the following area I need to Improve.

1. I always try to do my work quickly so that I make minor mistakes regardless of the deadline.
    * In order to improve this, I will recheck my work as many times as possible, if I had time.

2. I am always in a hurry to solve the problem without carefully listening to the question or instructions.
    * In order to improve this, I will pay close attention to the question or instructions, and only if I have a good understanding of the question will I proceed with the problem solving.

