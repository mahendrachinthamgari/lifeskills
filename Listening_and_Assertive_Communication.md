# Listening and Assertive Communication

### What are the steps/strategies to do Active Listening?

 **Active Listening**

: The act of fully hearing and comprehending the meaning of what someone is saying.

**Steps to active llistening**

* Avoid getting distracted by your own thoughts; instead, focus on the speaker and topic.
* Try not to interrupt the speaker; let them finish before responding.
* Use door openers; show you are interested, and keep the other person talking.
* Show that you are listening with your body language.
* Take appropriate notes during important conversations; paraphrase what others have said to make sure you're both on the same page.
* Provide feedback to the speaker at the end of the conversation.

### According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)?

**According to Fisher's model, these are the following are the key points of Reflective Listening.**

* We need to listen more than we need to speak. 
* Respond to the present content delivered by the speaker, not the content related to the topic or an unnecessary topic.
* Restating and clarifying what the other has said, not asking questions or telling what the listener feels, believes, or wants.
* Trying to understand the feelings contained in what the other 
is saying, not just the facts or ideas.
* Working to develop the best possible sense of the other's frame of reference while avoiding the temptation to respond from the listener's frame of reference.
* Responding with acceptance and empathy, not with indifference, cold objectivity, or fake concern.

### What are the obstacles in your listening process?

While I am listening, I continuously distracted with personal thoughts.

###  What can you do to improve your listening?

* Not being distracted by my own thoughts.
* Listening actively to the speaker with body language.
* Summirizing the content delivered by the speaker.
* Taking notes of important points while listening.

### When do you switch to Passive communication style in your day to day life?

* I will switch to Passive communication style when I am speaking with my boos or how has the power to change my present-future.
* I will switch to Passive communication style when I am speaking to my loved ones.
* I will switch to Passive communication style whwn I made a mistake and someone effected by that mistake.

### When do you switch into Aggressive communication styles in your day to day life?

* I will switch to Aggressive communication style when I don't make a mistake and someone blame's me for that mistake.
* I will switch to Aggressive communication style when I got irretediated by my subordinates.
* I will switch to Aggressive communication style when the things are not going well.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

* I will switch to Passive Aggressive style, when I am speaking with my friends and loved ones during relaxation.
* I will switch to Passive Aggressive style, when I am speaking with my colleague.

### How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? 

**Assertive Communication**

: Assertive Communication is expressing our feelings and needs clearly and honestly, while respecting the feelings and needs of others.

I can make my Assertive Communication by following these 5 steps:
* Learn to recognize and name your feelings.
* Learn to recognize and name what you want.
* Start with low-stakes suitation:- whenever I feel discomfort, I will tell them that.
* Be aware of your body language:- Sometimes body language makes a big difference; if I speak assertively, my body language may cause it to become aggrasive or passive. 
* Don't wait:- Every person has limitations, just as I do.