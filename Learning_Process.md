# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### Question 1

#### Feynman Technique

: Named after the famous physicist Richard Feynman, Explaining a concept works to improve your understanding of that concept. Feynman Technique can be used any field, eg:- Physics, Mathematics, History, Web Development, etc.,

They are 4 key steps to the Feynman Technique, they are:

1. Take a piece of paper and write the concept's name at the top.
2. Explian the concept using simple language.
3. Identify problem areas, then go back to the sources to review.
4. Pinpoint any complicated terms and challenge yourself to simplify them.

#### Parapharse in my own words

In the video, he is explaining how to improve our understanding of concepts easily using "Feynman Technique". The "Feynman Technique" is named after famous physicist, it doesn't mean this technique can be used only in math and science, but we can use this techinque in any field. When the concept is difficult to understand, By follwing these simble steps we can understand the concepts easily.

1. #### Take a piece of paper and write the concept's name at the top

When the concept is difficult to understand, first write the name of the concept on the top of a piece of paper.

2. #### Explian the concept using simple language

We have to explain the concept in our own simple words. The idea here is to do it in a way that's easy to understand, as if we were teaching someone else. We do not settle with defining the concept but also work through examples and make sure we are able to use the concept in practise as well.

3. #### Identify problem areas, then go back to the sources to review

We need to review our explanation and identify the areas where we didn’t know something or where we feel our explanation is shaky. Once we find out, then we have to go back to the source material, our notes, or any examples we can find in order to support our understanding.

4. #### Pinpoint any complicated terms and challenge yourself to simplify them

If there are any areas in our explanation where we use lots of technical terms or complex language, re-write these sections in simpler terms. We have to make sure our explanation could be understood by someone without the knowledge base we believe we already have.

#### Conclusion

If you are facing difficulty in understanding the concepts, then you are recommend you to follow Feynman Technique steps clearly, so that you can understand concept easily.

### Question 2

I can implement this technique in my learning process in different ways:

* Whenever I have difficulty understanding concepts, I will follow the Feynman Technique to improve my understanding.
* If I have to give seminors, then I would follow Feynman's technique to understand the topic better. so that I can confidently deliver the topic.
* If I find any complex words, then I will break them down into simple words so that I can understand them easily.
* I will explain concepts to myself so that I can easily understand them and be confident on that topic.

## 2. Learning How to Learn TED talk by Barbara Oakley

### Question 3

In the video, the speaker(Barbara Oakley) is talking about some learning techniques for different types of people based on their capability of brain, to understand how our mind works, Barbara Oakley researched neuroscience and cognitive psychology.

According to Barbara Oakley, The brain has fundamentally two different modes.

1. The Focus Mode.
2. The Diffuse Mode.

In Focus Mode, the neural states are together, focusing on the specific subject, whereas in diffuse mode, the neural states are relaxed. However, while learning, our mind does not stay in any particular state but goes back and forth between these two states. Although this is normal, at times we tend to procrastinate things, and this is because we feel a neural pain from how much effort that task needs to get over with, and eventually we end up doing what makes our brain feel light and happy, which makes it get distracted from learning. Hence, she suggested following the Pomodoro technique, wherein we decide to have a mindset that we can learn for 25 minutes with full focus and take rest for the next 5 minutes. This 25 minutes of full focus is achieved by keeping ourselves away from any kind of distraction, like mobile pings, and this period is not to get all our work completed within 25 minutes but to do the learning mindfully. This way, we are enhancing our ability to pay focused attention while learning.

Sometimes how much ever we do focus we tend to get distracted often and this is often with creative brains as they tend to think much more than what is being learnt. And some people consider themselves slow thinkers but they have a better grasp towards the concepts firmly than rushing over the concepts.

At times we feel that we have covered the concepts, but while testing we get jitters. So to avoid such anxiety, it is always better to test oneself as many times as possible. Try to work on the concept more and more until you are familiar with it. Because the more often you come across the concept, the more often you can recall it, and the more you master the subject. This is because our brain knows the flow of how to understand the concept quicker.

#### Conclusion

Never just work on a homework problem once and then put it away. Put yourself to the test by working through that homework problem several times over several days until the solution flows like a song from your mind.

### Question 4

These are the steps that I will take to improve my learning process. They are:

* I will follow the Pomodro technique while learning to improve my learning process.
* I will learn things with clarity rather than learning things quickly with a lack of clarity.
* When I'm resting, I'll put my newfound knowledge to the test.
* I will always understand the lessons and practise them with repetition.
* I will always recall my learnings before staring the new concept.

## 3. Learn Anything in 20 hours

### Question 5

These are the following are the key takeaways frm the video.

* Researchers found it takes 10,000 hours to become an expert in any new skill.
* The more we practice, the more we will become skilled at that skill.
* We will become good at things with just a little bit of practice.
* We can progress from knowing nothing about any skill to becoming experts in that skill.
* If we put in 20 hours of focused, deliberate practise to learn some new skills, we will be astounded.
* 4 simple steps to rapid skill acquition

    1. Deconstruct the skill: Decide exactly what we want to be able to do when we are done.
    2. Learn enough to self-correct: get three to five resources about what we are trying to learn.
    3. Remove practise barriers: a little bit of willpower can remove all distractions that are keeping us from practicing.
    4. Practice for at least 20 hours: Most skills have a frustration barrier, which we will be able to overcome by practising for at least 20 hours.
* The major barrier to skill acquisition isn't intellectual; it's emotional.
* Feeling stupid doesn't feel good in the beginning of learning anything new.

### Question 6

* I will deecide exactly what I want to be able to do when i am done.
* I will learn that new topic enough to self-correct.
* By exercising my willpower, I avoid distractions.
* I will practice more often.
* I will follow 4 simple steps to rapid skill acquition.
