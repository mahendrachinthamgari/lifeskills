# **Cache**

## **Cache Definition:**
1. A cache is a storage location used to temporarily store data used by servers, apps, and browsers to speed load times. Depending on where you look for them, almost any machine, will have and use a kind of cache.

2. The cache is a smaller and faster memory that stores copies of the frequently used data. Content like HTML pages, images, files, web objects, etc is stored in the cache to improve the efficiency and overall performance of the application.

3. Cache is a limited storage memory location, used to temporarily store data for faster loading.

## **Uses of Caching:**
Caches are widely used in most of the high volume applications:

* Reduce Latency 
* Increase Capacity
* Improve App Availability

## **Different Caching Approaches:**
The following are the different types of Caching approaches:

* [Web Caching (Browser/Proxy/Gateway)](#browser-caching)
* [Database Caching](#database-caching)
* [Content Delivery Network (CDN) Caching](#content-delivery-network-cdn-caching)
* [Application/Output Caching](#applicationoutput-caching)
* [Spatial Caching](#spatial-caching)
* [Temporal Caching](#temporal-caching)
* [Distributed Caching](#distributed-caching)
* [General Caching](#general-caching)

## **Web Caching(Browser/Proxy/Gateway):**
Caching web content helps imporve upon the responsiveness of websites by reducing the load on backed resources and network congestion. Web Caching is performed by retaining HTTP responses and web resources in the cache for the purpose of fulflling future requests from cache rather than the origin servers. Web Caching helps reduce overall network traffic and latency.
* Web Caching further divided into Two types:
1. Browser Caching
2. Proxy and Gateway cache

### Browser Caching:
Browser Caching helps individual users to quickly navigate pages they have recently visited. This process requires Cache-Control and ETag headers to be present to instruct the user's browser to cache certain files, for a certain period of time.

### Proxy and Gateway Cache:
Proxy and Gateway Cache allow cached information to be shared across larger groups of users. Data that does not change frequently and can be cached for longer periods of time is cached on Proxy and Gateway servers E.g.:DNS data that resolve domain names  to the IP address.

![Web Caching](https://d1.awsstatic.com/product-marketing/caching-database-caching-diagram.70a0c9d62877d7a32bf1024d00561eb5b560a45d.PNG)


## **Database Caching:**
Database Caching stores the data in local memory on the server and helps avoid extra trips to the Database for retrieving Data that has not changed. For  most Database solutions, cache frequently used queries in order to reduce turnaround time.

* Database Caching is very important for Database driven applications.
* It is standard practice to clear any cache data after it has been altered.
* Overuse of Database Caching can cause memory issues if data is constantly added and removed to and from the cache.

![Database Caching](https://d1.awsstatic.com/product-marketing/caching-database-caching-diagram.70a0c9d62877d7a32bf1024d00561eb5b560a45d.PNG)

## **Content Delivery Network (CDN) Caching:**
A Content Delivery Network (CDN) Caching is a critical component of nearly modern web application. It used to be that CDN merely improved the delivery of content by replicating commonly requested files (static content) across a globally distributed set of caching servers. However, CDNs have become much more useful over time. For caching, a CDN will reduce the load on an application origin and improve the experience of the requestor by delivering a local copy of the content from a nearby cache edge, or Point of Presence (PoP). The application origin is off the hook for opening the connection and delivering the content directly as the CDN takes care of the heavy lifting. The end result is that the application origins don’t need to scale to meet demands for static content. E.g.:Amazon CloudFront is a large scale, global, and feature rich CDN that provides secure, scalable and intelligently integrated application delivery.
 
 * CDNs do a lot od more than just a caching, now they deliver dynamic content that is unique to the requests and not cacheable.
 * The advantage of having a CDN deliver dynamic content is application performance and scaling.

 ![CDNs Caching](https://d1.awsstatic.com/product-marketing/Cloudfront-cdn-diagram-v2.1a4a693fc371d29d794d318a01dfe1aecc4c658e.PNG)

## **Application/Output Caching:**
While Database Caching stores raw data sets, Application/Output Caching utilizes server-level caching techniques that cache raw HTML. It can be a page or parts of a page (headers/footers), but it is usually HTML markup.

This type of caching can drastically reduce website load time and reduce server overhead.

## **Spatial Caching:**
Spatial Cache means that, all those instructions are stored nearby to the recently executed instruction have high chances of execution. It refers to the use of data elements (instructions) which are relatively close in storage location.

* Spatial Caching is a locality based Caching.

![Spatial Caching](https://media.geeksforgeeks.org/wp-content/uploads/Untitled-Diagram4.jpg)

## **Temporal Caching:**
Temporal Caching means, that a instruction which is recently executed have high chances of execution again. So the instruction is kept in cache memory such that it can be fetched easily and takes no time in searching for the same instruction. Least recent used cache are discarded and most recently used cache are stored with higher priority.

* Temporal Caching is based on locality and time.

![Temporial Caching](https://media.geeksforgeeks.org/wp-content/uploads/Untitled-Diagram3.jpg)

## **Distributed Caching:**
A Distributed Caching is a cache shared by multiple app servers, typically maintained as an external service to the app servers that access it, A distributed cache can improve the performance and scalability of an ASP.NET Core app, especially when the app is hosted by a cloud service or a server farm.

* Distributed Caching is based on both Spatial and Temporal Caching.
* High volume applications such as Google, YouTube and Amazon use Clustered Distributed Caching.
* A new machine of memory can be added at any time without disrupting users.
* Allows the web servers to pull and store from distributed server’s memory.
* Allows the webserver to simply serve pages and not have to worry about running out of memory.

![Distributed Caching](https://hazelcast.com/wp-content/uploads/2021/12/39_Distributed-Cache.png)

## **General Caching:**
There may be arbitrary areas where you may need to store and retrieve data not suitable for a traditional database in order to efficiently and rapidly respond to a request or compute the response. For example, say your application receives some user input and needs to check whether a similar request was made within the hour before responding back. One way to approach this would be to cache your users requests for an hour and code your application to check that cache when a new user request is made. You may also have other types of data that’s cached that could help compute a value used in the response. This type of cached information is transient and requires extremely low latency.
 
![General Caching](https://d1.awsstatic.com/product-marketing/caching-general-cache-diagram.f5a020839ec8e7ee3067b68fa50ecffccac8b6d4.PNG)


## **Reference**
* https://aws.amazon.com/caching/
* https://www.geeksforgeeks.org/difference-between-spatial-locality-and-temporal-locality/
* https://www.youtube.com/watch?v=ccemOqDrc2I
* https://aws.amazon.com/caching/best-practices/
* https://bootcamp.uxdesign.cc/caching-techniques-one-should-know-603e09d2b298
* https://learn.microsoft.com/en-us/aspnet/core/performance/caching/distributed?view=aspnetcore-7.0